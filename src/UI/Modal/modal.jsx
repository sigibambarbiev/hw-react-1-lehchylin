import {Component} from "react";
import s from "./Modal.module.scss"

class Modal extends Component {
    constructor(props) {
        super(props);
        this.header = props.header;
        this.closeButton = props.closeButton;
        this.text = props.text;
        this.actions = props.actions;
        this.openModal = props.openModal;
        this.onClose = props.onClose;
    }

    render() {
        // const {openModal} = this.props;

        return (
            <>
                {this.props.openModal &&
                    (<div className={s.wrapperModal}>
                            <div className={s.overlayModal} onClick={this.props.onClose}/>
                            <div className={s.contentModal}>
                                <div className={s.headerWrapper}>
                                    <h3 className={s.headerModal}>{this.props.header} </h3>
                                    {this.props.closeButton && <button className={s.modalCloseButton} onClick={this.props.onClose}>X</button>}
                                </div>
                                <div className={s.textModal}>{this.props.text} </div>
                                <div className={s.actionModal}>{this.props.actions} </div>
                            </div>
                        </div>
                    )
                }
            </>
        )
    }
}

export default Modal