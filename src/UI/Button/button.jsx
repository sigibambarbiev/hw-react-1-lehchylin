import {Component} from "react";
import s from './Button.module.scss';


class Button extends Component {
    constructor(props) {
        super(props);
        this.onClick = props.onClick;
        this.text = props.text;
        this.backgroundColor = props.backgroundColor;
    }

    render() {
        return (
            <button className={s.btn} onClick={this.props.onClick} style={{backgroundColor: this.backgroundColor}}>
                {this.text}
            </button>
        )
    }
}

export default Button
