import {Component} from "react";
import Button from "./UI/Button/button";
import Modal from "./UI/Modal/modal";

import s from './App.module.scss'

class App extends Component {
    state = {
        openModal: false,
        header: '',
        closeButton: false,
        text: '',
        actions: <></>
    }

    showModalFirst = () => {
        this.setState({
            openModal: true,
            header: "Do you want to delete this file?",
            text: "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?",
            closeButton: true,
            actions: <>
                <Button text={'Ok'}
                        backgroundColor={'#cb3131'}
                        onClick={this.closeModal}>
                </Button>
                <Button text={'Cancel'}
                        backgroundColor={'#cb3131'}
                        onClick={this.closeModal}>
                </Button>
            </>,
        })
    }

    showModalSecond = () => {
        this.setState({
            openModal: true,
            header: "Do you want to save changes to this file?",
            text: "Once you modify this file, this action cannot be undone. Are you sure you want to change this?",
            closeButton: true,
            actions: <>
                <Button text={'Save Change'}
                        backgroundColor={'#cb3131'}
                        onClick={this.closeModal}>
                </Button>
                <Button text={'Reset'}
                        backgroundColor={'#cb3131'}
                        onClick={this.closeModal}>
                </Button>
            </>,
        })
    }

    closeModal = () => {
        this.setState({
            openModal: false,
        })
    }

    render() {
        return (
            <div className={s.app}>
                <Button
                    onClick={this.showModalFirst}
                    text={'Open First Modal'}
                    backgroundColor={'#1d72b4'}>
                </Button>
                <Button
                    onClick={this.showModalSecond}
                    text={'Open Second Modal'}
                    backgroundColor={'#4fbb51'}>
                </Button>
                <Modal openModal={this.state.openModal}
                       header={this.state.header}
                       text={this.state.text}
                       actions={this.state.actions}
                       onClose={this.closeModal}
                       closeButton={this.state.closeButton}>
                </Modal>
            </div>
        )
    }
}


export default App;
